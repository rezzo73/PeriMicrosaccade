%% create a database of changes for each subject.
% Naghmeh Mostofi
% last updated: Aug. 19, 2015
% use NM calculated velocity for Sac detection instead of EMAT
% also use data with corrected Saccade start and end

clc
clear all
close all

NoSbj = 1; %6;
%SbjName = ['CS';'CT';'KS';'NM';'SA';'TS'];
SbjName = 'NM';
HorAngBorder = 45;

for ss = 1:NoSbj
    Sbj = SbjName(ss,:);
    DataFileName = ['/PMV_VT_' Sbj '_VelNM_RT_CorrSac'];
    DataPath = ['/Volumes/APLAB/BACKUPS/Mostofi_Backup/backup_Sept2016/projects/PeriMsacV/Matlab codes and data/Data/DatabaseswithNewVelocityEstimation/' Sbj, DataFileName '.mat'];
    %DataPath = ['C:\aplab\Naghmeh\MATLAB\PeriMicrosaccadicVision\Data\DatabaseswithNewVelocityEstimation\' Sbj];
%     load(DataFileName);
    load(DataPath);
    
    NoTrials = length(VT);
    clear Chng 
    cc = 0; % change counter per sbj
    
    for tn = 1:NoTrials
       
        % merge saccades and microsaccades
        Sac.start = [VT{tn}.microsaccades.start VT{tn}.saccades.start];
        Sac.duration = [VT{tn}.microsaccades.duration VT{tn}.saccades.duration];
        Sac.end = Sac.start + Sac.duration -1; 
        
        Sac.amplitude = [VT{tn}.microsaccades.amplitude VT{tn}.saccades.amplitude];
        NoSac = length(Sac.start);
        
%         [VX,BX] = sgfilt(VT{tn}.x.position,3,41,1);
%         [VY,BY] = sgfilt(VT{tn}.y.position,3,41,1);
%         Vel = sqrt(VX.^2 + VY.^2);
        Vel = VT{tn}.velocity ./1000; %arcmin/ms
        
        if (~isempty(VT{tn}.ChangeTimePres))
            
            NoChng = length(VT{tn}.ReactionTime);

            for cn=1:NoChng

                cc = cc+1;
                ChngT = round(VT{tn}.ChangeTimePres(cn));

                x=VT{tn}.x.position(ChngT)+VT{tn}.corrX;
                y=VT{tn}.y.position(ChngT)+VT{tn}.corrY;

                ChangeObjId = VT{tn}.ChangeObjId(cn)+1;
                ObjX = VT{tn}.ObjPos_X(ChangeObjId);
                ObjY = VT{tn}.ObjPos_Y(ChangeObjId);

                ChngAng = mod(atan2(ObjY-y,ObjX-x)*180/pi,360);
                Chng{cc}.ChngDist = sqrt((x-ObjX)^2+(y-ObjY)^2);
                Chng{cc}.ChngSize = VT{tn}.ChangeSize(cn);
                
                Chng{cc}.ReactionTime = nan; %default
                
                for kk=1:length(VT{tn}.HitReactionTime.ChageID)                    
                    if cn == VT{tn}.HitReactionTime.ChageID(kk)
                        Chng{cc}.ReactionTime =  VT{tn}.HitReactionTime.RT(kk);
                    end
                end
                
                % save the velocity of the eye at the time of change
                Chng{cc}.EyeVelAtChngOnset = Vel(ChngT);  %arcmin/ms
                Chng{cc}.AveEyeVelDurChng = mean(Vel(ChngT:ChngT+10)); %arcmin/ms
                
                % find the corresponding saccade to the change
                % the corresponding saccade is the one closest to the
                % change, considering the start ot the end of the saccades.
                
                tmpS = ChngT - Sac.start;
                tmpE = ChngT - Sac.end;
                mtmpS = min(abs(tmpS));
                mtmpE = min(abs(tmpE));
                
                if mtmpS<=mtmpE
                    CorrSacId = find(abs(tmpS) == mtmpS ,1); 
                else
                    CorrSacId = find(abs(tmpE) == mtmpE ,1); 
                end
                
                Chng{cc}.TimeToCorrSac = ChngT - Sac.start(CorrSacId);
                Chng{cc}.CorrSac_StartT = Sac.start(CorrSacId);
                Chng{cc}.CorrSac_Amplitude = Sac.amplitude(CorrSacId);
                Chng{cc}.CorrSac_Duration = Sac.duration(CorrSacId);
                Chng{cc}.CorrSacId = CorrSacId;
                Chng{cc}.CorrSacLandPos_X = VT{tn}.x.position(Chng{cc}.CorrSac_StartT + ...
                    Chng{cc}.CorrSac_Duration -1)+VT{tn}.corrX;
                Chng{cc}.CorrSacLandPos_Y = VT{tn}.y.position(Chng{cc}.CorrSac_StartT + ...
                    Chng{cc}.CorrSac_Duration -1)+VT{tn}.corrY;
                Chng{cc}.ChngDistToLanPos = sqrt((Chng{cc}.CorrSacLandPos_X-ObjX)^2 + ...
                    (Chng{cc}.CorrSacLandPos_X-ObjY)^2);
                
                % find the time of peak velocity of the corresponding
                % saccade.
                SacV = Vel(Chng{cc}.CorrSac_StartT:Chng{cc}.CorrSac_StartT+Chng{cc}.CorrSac_Duration-1);
                PSV = max(SacV);
                PSVT = find(SacV == PSV);
                
                Chng{cc}.CorrSac_PeakVel = PSV; %arcmin/ms
                Chng{cc}.CorrSac_PeakVelTime = PSVT + Chng{cc}.CorrSac_StartT - 1;
                Chng{cc}.TimeToCorrSacPeakVel = ChngT - Chng{cc}.CorrSac_PeakVelTime;                      
                
    %             % find the proceeding saccade
    %             tmpp = Sac.start - ChngT;
    %             ProcSacId = find( tmpp>0 & tmpp == min(abs(tmpp)) ,1); 
    %             Chng{cc}.TimeToProcSac = Sac.start(ProcSacId)-ChngT;
    %             Chng{cc}.ProcSac_StartT = Sac.start(ProcSacId);
    %             Chng{cc}.ProcSac_Amplitude = Sac.amplitude(ProcSacId);
    %             Chng{cc}.ProcSac_Duration = Sac.duration(ProcSacId);
    %             Chng{cc}.ProcSacId = ProcSacId;

                %check if the change is horizontal
                Chng{cc}.IsHor = (ChngAng>0 & ChngAng<HorAngBorder) | (ChngAng>(180-HorAngBorder) ...
                    & ChngAng<(180+HorAngBorder)) | (ChngAng>(360-HorAngBorder) & ChngAng<360);

                % Check if the change is temporal/nasal
                Chng{cc}.IsTemporal = (ChngAng>=0 & ChngAng<=90) | (ChngAng>=270 & ChngAng<=360);

                % save in the str

                Chng{cc}.Sbj = Sbj;
                Chng{cc}.id = VT{tn}.id; % edit
                Chng{cc}.TrialId = tn;
                Chng{cc}.ChngId = cn;
                Chng{cc}.ChngTime = ChngT;
                Chng{cc}.GazePos_X = x;
                Chng{cc}.GazePos_Y = y;
                Chng{cc}.ChngObjPos_X = ObjX; % edit
                Chng{cc}.ChngObjPos_Y = ObjY; % edit  
                %Chng{cc}.ObjPos_X = VT{tn}.ObjPos_X; % edit
                %Chng{cc}.ObjPos_Y = VT{tn}.ObjPos_Y; % edit
                %Chng{cc}.background = VT{tn}.imgBack; % edit                
                Chng{cc}.ChngAng = ChngAng;
                Chng{cc}.AllSac = Sac; 
                
                
            end
        end   
    end
    ChangeDB{ss} = Chng;
end

save('/Volumes/APLAB/BACKUPS/Mostofi_Backup/backup_Sept2016/projects/PeriMsacV/Matlab codes and data/Data/DatabaseswithNewVelocityEstimation/ChangeDB2','ChangeDB');

