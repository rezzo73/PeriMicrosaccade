%Computing average of imgBCK 11x11 pixel area around target and 
%excluding target contrast values
%clear all
%clc

Threshold = 8; %this is for 8 + (1) + 8 pixel width & length (17x17 area)
%Threshold = 5;
subject = 'SA';
%load('/Volumes/APLAB/BACKUPS/Mostofi_Backup/backup_Sept2016/projects/PeriMsacV/Matlab codes and data/Data/NM/ChangeDB_backgroundandfleas.mat');

for di = 1:length(ChangeDB{1,1})

RGB_Noise = imread(sprintf(['/Volumes/APLAB/BACKUPS/Mostofi_Backup/backup_Sept2016/projects/PeriMsacV/Matlab codes and data/' ChangeDB{1,1}{1,di}.background]));

c=[1:600];
r=[1:800];
%RGB_P = impixel(RGB_Noise,2,1); % RGB value
Pixel_luminance = single(RGB_Noise(c,r)); %pixel value; luminance?
S = size(Pixel_luminance);
Center = S/2;

%adding the other black squares:
    black_squares = zeros(600,800);
    blacksquare_x_locations = zeros(30,1)';
    blacksquare_y_locations = zeros(30,1)';
    
    for ci = 1:30
        blacksquare_x_locations(ci) = (Center(2) ...
        + ChangeDB{1,1}{1,di}.ObjPos_X(ci));
        blacksquare_y_locations(ci) = (Center(1) + ...
         ChangeDB{1,1}{1,di}.ObjPos_Y(ci));

       if blacksquare_y_locations(ci) == ChangeDB{1,1}{1,di}.ChngObjPos_Y + 300 && ...
          blacksquare_x_locations(ci) == ChangeDB{1,1}{1,di}.ChngObjPos_X + 400 
          black_squares(blacksquare_y_locations(ci)-2 : blacksquare_y_locations(ci)+2, ...
          blacksquare_x_locations(ci)-2 : blacksquare_x_locations(ci)+2) = 0;
      continue;
       else
        black_squares(blacksquare_y_locations(ci)-2 : blacksquare_y_locations(ci)+2, ...
        blacksquare_x_locations(ci)-2 : blacksquare_x_locations(ci)+2) = NaN; %had to switch x & y here
       end
    end

%include black squares in background calculation
Final_background = Pixel_luminance + black_squares;
Final_background(isnan(Final_background)) = 50;


%Now isolate 11x11 pixel area & calculate the background value with incorporated squares   
    ChangeDB{1,1}{1,di}.bckgd_value = [];
    Flash_Location = [(Center(2) ...
        + ChangeDB{1,1}{1,di}.ChngObjPos_X), ...
        (Center(1) + ChangeDB{1,1}{1,di}.ChngObjPos_Y)]; %make sure x & y are correct
   
    %took out due to combine two large matrices instead 
    Small_Matrix = Final_background(Flash_Location(1)-Threshold : Flash_Location(1)+Threshold, ...
        Flash_Location(2)-Threshold : Flash_Location(2)+Threshold); %%%%JUST CHANGED THIS
    
    Small_S = size(Small_Matrix);
    Small_Center = round(Small_S/2);
    
    Small_Matrix(Small_Center(1)-2 : Small_Center(1)+2, ...
        Small_Center(2)-2 : Small_Center(2)+2) = NaN;   
    
    Average_Intensity = mean(Small_Matrix(~isnan(Small_Matrix)));
    Average_Luminance = (0.0551 * Average_Intensity) + 0.003;
    
    ChangeDB{1,1}{1,di}.bckgd_value = Average_Intensity;
    ChangeDB{1,1}{1,di}.bckgd_luminance = Average_Luminance;
    
    ChangeDB{1,1}{1,di}.Min_Lum = (0.0551 * 50) + 0.003;    
    ChangeDB{1,1}{1,di}.Max_Lum = (0.0551 * (ChangeDB{1,1}{1,di}.ChngSize + 50)) + 0.003;
    
    
    
    %%took out due to combine two large matrices instead
    
end






