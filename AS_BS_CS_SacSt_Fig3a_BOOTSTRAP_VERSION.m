%%AS_BS_CS_SacSt_Fig3a_NM_LumMax_minus_LumMin
%this version calculates Resp based on the fomula Min_Lum-Max_Lum


%% Naghmeh Mostofi
% last updated: Aug. 25, 2016.

% fit the psychometric function to the data for each individual subject in
% different temporal and spatial bins.
% used Michelson contrast.
%hits are defined as the changes that were detected within 300 to 1000 ms.
% the code plots Fig3A of the manuscript
% use velocity estimated by NM instead of EMAT
% the start and end of the saccades have been corrected


%cd('C:\APLab\Naghmeh\MATLAB\PeriMicrosaccadicVision')

%HorAngBorder = 45;
%DataFileName = ['ChangeDB_' num2str(HorAngBorder) 'deg'];
DataFileName = ['ChangeDB_with_LuminanceContrast'];

%dataPath = ['/Volumes/APLAB/BACKUPS/Mostofi_Backup/backup_Sept2016/projects/PeriMsacV/Matlab codes and data/Data/DatabaseswithNewVelocityEstimation/' DataFileName '.mat'];
dataPath = ['Z:\BACKUPS\Mostofi_Backup\backup_Sept2016\projects\PeriMsacV\Matlab codes and data\Data\NM\' DataFileName '.mat'];
load(dataPath)
%ChangeDB = ChangeDB(4);% subject 4 = NM
NoSbj = length(ChangeDB_with_LuminanceContrast); 

PlotCSF = 1;
Threshlev = 0.25;

SacAmpR_min = 3;
SacAmpR_max = 60; % arbitrary large number
MinRT = 300;
MaxRT = 1000;
bootRepeat = 2000; % Naghmeh used 2000 but this was taking too long to run

%% initialize
SpVec = [0:15:30 60];
TmpVec = [-200 -100 -50 -25 0 25 50 100 200];
TimeDisc = 200;

for sb=1:NoSbj
    
    data = ChangeDB_with_LuminanceContrast{sb};
    Resp = cell(length(SpVec)-1,length(TmpVec)-1,2);
    ChTToSacOn = cell(length(SpVec)-1,length(TmpVec)-1);
    NoChngUsed=0;
    clear UsedChng    
    
    for cn=1:length(data)
        
        % only check the horizontal saccades
        
        if (data{cn}.IsHor)
            
            if data{cn}.CorrSac_Amplitude >= SacAmpR_min && data{cn}.CorrSac_Amplitude <= SacAmpR_max
                
                Chngdist = data{cn}.ChngDist;

                % for changes with time difference to corresponding saccade smaller
                % than TimeDisc, we have to make sure that there are no other
                % saccades within this range. 

                TimeToCorrSac = data{cn}.TimeToCorrSac;

                if abs(TimeToCorrSac)<=TimeDisc

                    if (sum(abs(data{cn}.ChngTime - data{cn}.AllSac.start)<=TimeDisc)==1)

                        % now assign the valid change to the correct spatiotemporal
                        % window

                        for sv = 1:length(SpVec)-1
                            sb_min = SpVec(sv);
                            sb_max = SpVec(sv+1);

                            if Chngdist>= sb_min && Chngdist< sb_max

                                for tv=1:length(TmpVec)-1
                                    tb_min = TmpVec(tv);
                                    tb_max = TmpVec(tv+1);

                                    if TimeToCorrSac>= tb_min && TimeToCorrSac< tb_max
                                        ChTToSacOn{sv,tv} = [ChTToSacOn{sv,tv} TimeToCorrSac];
                                        Resp{sv,tv,1} = [Resp{sv,tv,1} round((data{cn}.Max_Lum - data{cn}.Min_Lum)/(data{cn}.Max_Lum + data{cn}.Min_Lum) ,2)];
                                        %%Resp{sv,tv,1} = [Resp{sv,tv,1} round((data{cn}.Max_Lum - data{cn}.Min_Lum) ,1)];

                                        ishit = (data{cn}.ReactionTime>=MinRT && data{cn}.ReactionTime<=MaxRT);
                                        Resp{sv,tv,2} = [Resp{sv,tv,2} ishit];
                                        NoChngUsed=NoChngUsed+1;
                                        UsedChng(NoChngUsed) = data{cn};
                                    end
                                end
                            end 
                        end
                    end
                end
            end
        end
    end
    Performance{sb} = Resp;
    ChngTimeToSacOn{sb} = ChTToSacOn;
    NoUsedChng{sb} = NoChngUsed;
    UsedChngDB{sb} = UsedChng; 
end

%% now plot the psychometric function for each spatiotemporal bin
thresh = zeros(length(SpVec)-1,length(TmpVec)-1,NoSbj);
GuessR = ones(length(SpVec)-1,length(TmpVec)-1,NoSbj);
IsGoodFit = [];


for sv = 1:length(SpVec)-1
    for tv=1:length(TmpVec)-1
        for sb=1:NoSbj
            Perf = Performance{sb};           

            if (length(Perf{sv,tv,1})>=30) 
                
                % fit psychometric function
                if PlotCSF
                    [th gR lR] = FitSigFun_cg(Perf{sv,tv,1},Perf{sv,tv,2} ,'Log','Thresh',Threshlev,'Extra');                       
                    thresh(sv,tv,sb)= th;  
                    GuessR(sv,tv,sb)= gR;
                    
                    %bootstrap 
                  
                        th_boot{sv,tv,sb} = bootstrp(bootRepeat,@FitSigFun_Bstrp,Perf{sv,tv,1},Perf{sv,tv,2});
                        ThreshBoot = th_boot{sv,tv,sb};
                   
                        
                        
                    SbjN = ChangeDB_with_LuminanceContrast{sb}{1}.Sbj;
                    title(sprintf(['Subj:' SbjN ',  spatial bin: %i-%i arcmin,  temporal bin: %i-%i ms'],...
                        SpVec(sv), SpVec(sv+1), TmpVec(tv), TmpVec(tv+1)),'fontsize',15)
                    set(gca,'fontsize',15)
                    %set(gca,'XTick',[50:20:200])
                    % changed here by MP 5/14/17
                    
                    
      IsGoodFit(sv,tv,sb) = 1;
      
                    %print2eps(sprintf('C:\Users\Chris Gill\Desktop\NM_Figures_Max_Sac_Amp_30\%i-%iarcmin%i-%ims.eps',...
                    %    SpVec(sv), SpVec(sv+1), TmpVec(tv), TmpVec(tv+1)));
                  
        %IsGoodFit(sv,tv,sb) = input('Is it a good fit? (0/1)');
                    
%                     %printing figures to folder(THIS DOESN'T WORK)
%                     pt = 'C:/Users/Chris Gill/Desktop/NM_Figures_Max_Sac_Amp_30/';
%                     svname = sprintf('%i-%iarcmin%i-%ims', SpVec(sv), SpVec(sv+1), TmpVec(tv), TmpVec(tv+1));
%                     fn = (pt svname '.fig');
%                     save(fn);
%                     
                
                else
                    [th gR lR] = FitSigFun_cg(Perf{sv,tv,1},Perf{sv,tv,2} ,'Log','Thresh',Threshlev,'Extra','PlotOff');          
                    thresh(sv,tv,sb)= th;  
                    GuessR(sv,tv,sb)= gR;
                    IsGoodFit(sv,tv,sb) = 1;
                    
                    %bootstrap
                        th_boot{sv,tv,sb} = bootstrp(bootRepeat,@FitSigFun_Bstrp,Perf{sv,tv,1},Perf{sv,tv,2});
                        ThreshBoot = th_boot{sv,tv,sb};
                     
                end
            end       
        end
    end
end

% the bad fits that should be redone
% IsGoodFit(sv,tv,sb)
IsGoodFit(3,6,4) = 0;


%% redo the fitting in the conditions with bad fit using a different start
% point in the findmin function. 
for sv = 1:length(SpVec)-1
    for tv=1:length(TmpVec)-1
        for sb=1:NoSbj
            
            if IsGoodFit(sv,tv,sb)==0;

                Perf = Performance{sb};
                
                if (length(Perf{sv,tv,1})>=30) 
                    [th gR lR] = FitSigFun_MaxSP_cg(Perf{sv,tv,1},Perf{sv,tv,2} ,'Log','Thresh',Threshlev,'Extra');                       
                    thresh(sv,tv,sb)= th;  
                    GuessR(sv,tv,sb)= gR;
                    
                    %bootstrap
                        th_boot{sv,tv,sb} = bootstrp(bootRepeat,@FitSigFun_MaxSP_Bstrp,Perf{sv,tv,1},Perf{sv,tv,2});
                        ThreshBoot = th_boot{sv,tv,sb};
                   
                        
                    SbjN = ChangeDB_with_LuminanceContrast{sb}{1}.Sbj;
                    title(sprintf(['Subj:' SbjN ',  spatial bin: %i-%i arcmin,  temporal bin: %i-%i ms'],...
                        SpVec(sv), SpVec(sv+1), TmpVec(tv), TmpVec(tv+1)),'fontsize',15)
                    set(gca,'fontsize',15)
                    %set(gca,'XTick',[50:20:200])
                    
                    IsGoodFit(sv,tv,sb) = input('Is it a good fit? (0/1)');
                    
                   
                end

            end
        end       
    end
end



thresh(find(thresh(:,:,:) >= 1)) = 1;

%% 
%%convert contrast to luminance

sens = 1./(thresh); %testing
%sens = (ThLum+2*BaseLum) ./ ThLum;  % Michelson contrast 
 for sv = 1:length(SpVec)-1
    for tv=1:length(TmpVec)-1
        for sb=1:NoSbj
            
            ttmpp = 1./th_boot{sv,tv,sb};
            ttmpp(find(ttmpp == Inf)) = nan;
            sens_boot{sv,tv,sb} = ttmpp;
        end
    end
end
%% plot==============================
% upper and lower confidence interval bounds

for sv = 1:length(SpVec)-1
    for tv=1:length(TmpVec)-1
        for sb=1:NoSbj
            lo_bnd(sv,tv,sb) = prctile(sens_boot{sv,tv,sb}, 2.5) - sens(sv,tv,sb);
            up_bnd(sv,tv,sb) = prctile(sens_boot{sv,tv,sb}, 97.5) - sens(sv,tv,sb);
        end
    end
end
%% plots

% plot contrast sensitivity functions for each subject
hfig = figure('PaperPosition',[0 0 8.5 11],'Color',[1 1 1]);
hold on
col = [1 0 0;0 0 1; 0 0.5 0];

for sb=1:NoSbj
    tmp = ChngTimeToSacOn{sb};
    for tv = 1:length(TmpVec)-1
        for sv = 1:length(SpVec)-1   
            tempTicks(sv,tv) = mean(tmp{sv,tv});
        end
    end
end

for sb=1:NoSbj
    SbjN = ChangeDB_with_LuminanceContrast{sb}{1}.Sbj;
    subplot(2,3,sb)  
    hold on
    clear pp ss
    for sv = 1:length(SpVec)-1 
        
        x = tempTicks(sv,:);
        pp(sv) = plot(x,sens(sv,:,sb),'-d','color',col(sv,:),'MarkerSize',12,'MarkerFaceColor',col(sv,:),'markerEdgeColor',col(sv,:));
        
        errorbar(x, sens(sv,:,sb), lo_bnd(sv,:,sb),up_bnd(sv,:,sb),'color',col(sv,:), 'linestyle', 'none'); %errorbar
        
        ss{sv} = sprintf(['%i-%i arcmin'],SpVec(sv), SpVec(sv+1));
        xlim([TmpVec(1) TmpVec(end)]);
        
        xlabel('time from saccade onset (ms)','FontWeight','bold','FontSize',24);
        ylabel('contrast sensitivity','FontWeight','bold','FontSize',24);
        set(gca,'XTick',TmpVec,'XTickLabel',TmpVec(1:end));
        set(gca, 'yscale', 'log','FontSize', 12)
        %set(gca,'YTick',1:0.5:3);
        ylim([0 2]);
        box off
        lb = legend(pp,ss,'Location', 'northeast');
        set(lb, 'FontWeight','bold','FontSize',18)
    end
    title (SbjN  ,'FontWeight','bold','FontSize',26);
    hold off 
end
%% 

% %% average across all subjects for each spatial bin
% 
% for sv = 1:length(SpVec)-1    
%     for sb=1:NoSbj        
%         tmpp(sb,:) = sens(sv,:,sb);
%     end
%     Sens_S{sv} =  tmpp;
%     
%     for qq=1:length(tmpp)
%         eee = tmpp(~isnan(tmpp(:,qq)),qq);
%         BS_MCS(sv,qq) = mean(eee);
%         BS_SECS (sv,qq) = std(eee)./sqrt(length(eee));
%     end
% end
% 
% % now put all 3 curves together
% hfig = figure('PaperPosition',[0 0 8.5 11],'Color',[1 1 1]);
% hold on
% for sv = 1:length(SpVec)-1 
%     x = tempTicks(sv,:);
%     pp(sv)=plot(x,BS_MCS(sv,:),'-d','color',col(sv,:),'MarkerSize',12,'MarkerFaceColor',col(sv,:),'markerEdgeColor',col(sv,:));
%     hold on
%     errorbar(x,BS_MCS(sv,:),BS_SECS(sv,:),'color',col(sv,:))    
%     ss{sv} = sprintf(['%i-%i arcmin'],SpVec(sv), SpVec(sv+1));
% end
% hold off 
% xlim([TmpVec(1) TmpVec(end)]);
% ylim([1.3 2.5]);
% xlabel('time from saccade onset (ms)','FontWeight','bold','FontSize',24);
% ylabel('contrast sensitivity','FontWeight','bold','FontSize',24);
% set(gca,'XTick',TmpVec,'XTickLabel',TmpVec(1:end));
% set(gca, 'yscale', 'log','FontSize', 24)
% set(gca,'YTick',1:0.5:3);
% box off
% lb = legend(pp,ss,'Location', 'northeast');
% set(lb, 'FontWeight','bold','FontSize',26)
% 
%% for each subject construct the 2D map

hfig = figure('PaperPosition',[0 0 8.5 11],'Color',[1 1 1]);
x=1:length(TmpVec)-1;
y=1:length(SpVec)-1;

for sb=1:NoSbj
    SbjN = ChangeDB_with_LuminanceContrast{sb}{1}.Sbj;
    subplot(2,3,sb)  

    imagesc(x,y,sens(:,:,sb));
    colormap hot
    xlim([x(1)-0.5 x(end)+0.5]);
    ylim([y(1)-0.5 y(end)+0.5]);
    xlabel('time from saccade onset(ms)','FontWeight','bold','FontSize',18);
    ylabel('eccentricity (arcmin)','FontWeight','bold','FontSize',18);
    set(gca, 'FontSize', 16)
    set(gca,'XTick',[x(1:end)-0.5 x(end)+0.5],'XTickLabel',TmpVec(1:end));
    set(gca,'Ydir','Normal')
    set(gca,'YTick',[y(1:end)-0.5 y(end)+0.5],'YTickLabel',SpVec(1:end));
    box off
    title (SbjN ,'FontWeight','bold','FontSize',18);
    caxis([0.7 3]);

    colorbar('YTick',1:0.5:3,'YTickLabel',1:0.5:3, 'FontSize', 16)
    axis square
    hold off  
end

% %% find average across all subjects
% hfig = figure('PaperPosition',[0 0 8.5 11],'Color',[1 1 1]);
% x=1:length(TmpVec)-1;
% y=1:length(SpVec)-1;
% 
% imagesc(x,y,BS_MCS);
% colormap hot
% xlim([x(1)-0.5 x(end)+0.5]);
% ylim([y(1)-0.5 y(end)+0.5]);
% xlabel('time from the start of saccade (ms)','FontWeight','bold','FontSize',24);
% ylabel('eccentricity (arcmin)','FontWeight','bold','FontSize',24);
% set(gca, 'FontSize', 22)
% set(gca,'XTick',[x(1:end)-0.5 x(end)+0.5],'XTickLabel',TmpVec(1:end));
% set(gca,'Ydir','Normal')
% set(gca,'YTick',[y(1:end)-0.5 y(end)+0.5],'YTickLabel',SpVec(1:end));
% box off
% caxis([1.1 2.5]);
% colorbar('YTick',1.5:0.5:2.5,'YTickLabel',1.5:0.5:2.5, 'FontSize', 24)
% axis square
% hold off  

%% statistics
% quantify the change within each spatial bin
% for sv = 1:length(SpVec)-1      
%     PerCh_S(sv,:) = BS_MCS(sv,:)./max(BS_MCS(sv,:));
% end
    
% % quantify the change within each temporal bin
% for tv = 1:length(TmpVec)-1      
%     PerCh_T(:,tv) = BS_MCS(:,tv)./max(BS_MCS(:,tv));
% end    

% statistics within each temporal bin
% for tv = 1:length(TmpVec)-1   
%     for sb=1:NoSbj        
%         ttmpp(sb,:) = sens(:,tv,sb);
%     end
%     if tv==4
%         ttmp(1:3,:) = ttmpp(1:3,:);
%         ttmp(4:5,:) = ttmpp(5:6,:);
%         ttmpp = ttmp;
%     end
%  
%     nf = (mean(ttmpp(:)) - (mean(ttmpp'))');
%     nttmpp = ttmpp + [nf nf nf];
%     nMttmpp = mean (nttmpp);
% 
%     [p,Table,stats] = anova2(nttmpp);
%     multcompare(stats)
% end

% statistics within each spatial bin
% one of the values for sv=3 is nan, so we only do the comparison between 5
% subjects
% for sv = 1:length(SpVec)-1 
%     if sv==3
%         tttmp = Sens_S{sv};
%         tttmpp(1:3,:) = tttmp(1:3,:);
%         tttmpp(4:5,:) = tttmp(5:6,:);
%         tmp = tttmpp;
%     else
%        tmp = Sens_S{sv};
%     end 
%     nf = (mean(tmp(:)) - (mean(tmp'))');
%     ntmp = tmp + [nf nf nf nf nf nf nf nf];
%     nMtmp = mean (ntmp);
% 
%     [p,Table,stats] = anova2(ntmp);
%     multcompare(stats)
% end